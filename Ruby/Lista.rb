class Node
  attr_accessor :value, :next_node
  def initialize(value, next_node)
    @value = value
    @next_node = next_node
  end
end


class List
  def initialize
    clear
  end

  def empty?
    @head.nil?
  end

  def clear
    @head = nil
    @tail = nil
    @n = 0
  end

  def get_first
    @head
  end

  def get_last
    @tail
  end

  def find(obj)
    n = @head
    while !n.nil? && n.value != obj
      n = n.next_node
    end
    n
  end

  def getPos(pos)
    cnt = 0
    n = @head
    while !n.nil? && cnt != pos
      cnt += 1
      n = n.next_node
    end
    n 
  end

  def each
    n = @head
    while !n.nil?
      yield n 
      n = n.next_node
    end
  end

  def size
    @n
  end

  def insert_first(obj)
    if empty?
      n = Node.new(obj, nil)
      @head = obj
      @tail = obj
    else
      n = Node.new(obj, @head)
      @head = obj
    end
    @n += 1
  end

  def insert_last(obj)
    if empty?
      insert_first(obj)
    else
      n = Node.new(obj, nil)
      @tail.next_node = n
      @tail = n
      @n += 1
    end
  end

  def insert_pos_rec(obj, pos, prev) #objeto, posiciones que faltan, nodo en que voy
    if prev.nil?
      return
    end
    if pos == 1
      n = Node.new(obj, prev.next_node)
      prev.next_node = n
      @n += 1
    else
      insert_pos_rec(obj, pos-1, prev.next_node)
    end
  end

  def insert_pos(obj, pos)
    if empty? || pos == 0
      insert_first(obj)
    else
      insert_pos_rec(obj, pos, @head)
    end
  end 

  def remove_rec(prev, obj)
    if prev.next_node.nil?
      return false
    end
    if  prev.next_node.value == obj
      prev.next_node  = prev.next_node.next_node
      @n -= 1
      true
    else
      remove_rec(prev.next_node, obj)
    end
  end

  def remove_obj(obj)
    if empty?
      puts "Lista vacia"
    elsif @head.value == obj
      @head = @head.next_node
      if @head.nil?
        @tail = nil
      end
      @n -= 1
      true
    else      
      deleteRec(@head, obj)
    end
  end

  def remove_obj_all(obj)
    cnt = 0
    while remove_obj(obj)
      cnt += 1
    end
    cnt
  end

  def remove_pos_rec(prev, pos)
    if pos == 0
      return
    end
    if pos == 1
      if !prev.next_node.nil?
        if prev.next_node == @tail
          @tail = prev
        end
        prev.next_node = prev.next_node.next_node
        @n -= 1
      end
    else
      remove_pos_rec(prev.next_node, pos-1)
    end
  end

  def remove_pos(pos)
    if pos == 0
      @head = @head.next_node unless @head.nill?
      @tail = nil if @head.nil?
      @n -= 1
    else 
      remove_pos_rec(@head, pos)
    end
  end

  def remove_first()
    remove_pos 0
  end

  def remove_last()
    remove_pos(size()-1)
  end

  def invertir_rec(nodo, aux)
    if nodo.nextNodo.nil?
      return
    else
      aux.insertFirst(nodo)
      invertir_rec(nodo.next_node, aux)
    end
  end

  def invertir(lista)
    if lista.size == 1
      return
    else
      invertir_rec(nodo = lista.head, aux = Lista.new)
    end
  end

  def show_list
    if empty?
      puts "Lista vacia"
    else
      puts "==== Lista ===="
      temp = @head
      i=1
      seguir = true
      while seguir 
        puts "Node #{i.to_s} is: #{temp.value.to_s} "
        temp = temp.next_node
        i+=1
        if temp.next_node.nil?
          seguir = false
        end
      end
    end
  end
end

lista = List.new
lista.insert_first(1)
lista.insert_pos(2,2)
lista.insert_pos(3,3)
lista.insert_pos(4,4)
lista.insert_pos(5,5)
lista.insert_last(6)
lista.show_list
lista.invertir(lista)