class Node 
  attr_accessor :contact, :left, :right
  def initialize(contact)
    @contact = contact
    @left = left
    @right = right
  end
end

class ABB
  attr_accessor :root

  def initialize
    @root = nil
  end

  def empty?
    @root.nil?
  end

  def insert_rec(elem, node)
    if elem < node.value
      return node.left = Node.new(elem, nil, nil) if node.left.nil? 
      insert_rec(elem, node.left)
    elsif elem > node.value
      return node.right = Node.new(elem, nil, nil) if node.right.nil? 
      insert_rec(elem, node.right)
    end
    return node   
  end

  def insert(elem)
    return @root = Node.new(elem, nil, nil) if empty?
    return insert_rec(elem, @root)
  end

  def search_rec(elem, node)
    return false if node.nil?
    return true if node.value == elem
    if elem < node.value
      search_rec(elem, node.left)
    elsif elem > node.value
      search_rec(elem, node.right)
    end
  end

  def search(elem)
    return false if empty?
    return search_rec(elem, @root)
  end

  def replace_parent(node)
    replacement = find_maximum(node.right)
    #
    #
    #
  end

  def remove_rec(elem, node)
    return false if node.nil?
    if node.value == elem
      if node.left.nil? && node.right.nil?
        node = nil
      elsif !node.left.nil? && node.left.nil?
        node = node.left
      elsif node.left.nil? && !node.left.nil? 
        node = node.right
      else 
        node = replace_parent(node)
      end
    elsif elem < node.value
      remove_rec(elem, node.left)
    else
      remove_rec(elem, node.right)
    end
    return true
  end

  def remove(elem)
    return false if empty?
    return remove_rec(elem, @root)
  end

  def preorder
    foo = lambda do |nodo|
      if nodo.nil? then return end
      print "#{nodo.value} "
      foo.call(nodo.left)
      foo.call(nodo.right)
    end
    foo.call(@root)
  end

  def inorder
    foo = lambda do |nodo|
      if nodo.nil? then return end
      foo.call(nodo.left)
      print "#{nodo.value} "
      foo.call(nodo.right)
    end
    foo.call(@root)
  end

  def postorder
    foo = lambda do |nodo|
      if nodo.nil? then return end
      foo.call(nodo.left)
      foo.call(nodo.right)
      print "#{nodo.value} " 
    end
    foo.call(@root)
  end

  def leafs_number
  end

  def tree_height
  end

  def node_height(node)
    aux = @root
    height = 0
    if node == aux.value
      return height
    elsif search(node) == false
      return false
    else
      until aux.value == node do
        height += 1
        if node < aux.value
          aux = aux.left
        elsif node > aux.value
          aux = aux.right
        end
      end
    end
    return height
  end

  def find_minimum(node = @root)
    return node if node.left.nil?
    find_minimum(node.left)
  end

  def find_maximum(node = @root)
    return node if node.right.nil?
    find_maximum(node.right)
  end
end