class Queue
  def initialize 
    @store = Array.new
  end

  def dequeue
    @store.shift #shift quita el elemento en la primera posición del array
  end

  def enqueue(element)
    @store.push(element)
    self
  end

  def sieze
    @store.size
  end
end